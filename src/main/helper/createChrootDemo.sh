#!/usr/bin/env bash

# based on output of "ldd /bin/bash" and some "experiments" ...

cd ~

mkdir -p temp/fs1/{bin,lib,lib/x86_64-linux-gnu,lib64,usr,var}
mkdir -p temp/fs2

cp -v /bin/{bash,ls} temp/fs1/bin

cp  /lib/x86_64-linux-gnu/libtinfo.so.6   \
    /lib/x86_64-linux-gnu/libdl.so.2      \
    /lib/x86_64-linux-gnu/libc.so.6       \
    /lib/x86_64-linux-gnu/libselinux.so.1 \
    /lib/x86_64-linux-gnu/libpcre2-8.so.0 \
    /lib/x86_64-linux-gnu/libpthread.so   \
    /lib/x86_64-linux-gnu/libpthread.so.0 \
    ~/temp/fs1/lib/x86_64-linux-gnu/

cp  /lib64/ld-linux-x86-64.so.2 ~/temp/fs1/lib64/

cd -

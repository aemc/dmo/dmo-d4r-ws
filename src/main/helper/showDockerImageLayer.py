#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
import os
import sys
import docker

docker_lib_missing=False

try:
    from docker import Client
except:
    try:
        from docker import APIClient as Client
    except:
        docker_lib_missing=True

if docker_lib_missing:
    print( "Could not load docker python library; please install docker-py or docker library" )
    sys.exit( 1 )

length = len( sys.argv )
if length == 1:
    print( "echo needs minimal one argument: name of Docker image" )
    sys.exit( 1 )
elif length > 2:
    print( "echo too much arguments" )
    sys.exit( 1 )

docker_image_name = sys.argv[ 1 ]

print( "############################################################" )
print( "### investigating Docker image " + docker_image_name )
print( "### begin" )
print( "" )

docker_image_metadata = Client().inspect_image( docker_image_name )

docker_image_id = docker_image_metadata[ "Id" ]
docker_image_id = docker_image_id[7:]

print( "docker-image-id: " + docker_image_id )

diff_ids_file = open( "/var/lib/docker/image/overlay2/imagedb/content/sha256/" + docker_image_id )

diff_ids = json.load( diff_ids_file )[ "rootfs" ][ "diff_ids" ]
# print "diff_ids: "
# print diff_ids

print( "\n==========\n" )

for id in diff_ids:
    print( "diff_id: " + id[7:] )
    cmd = "grep -r -i -n '" + id[7:] + "' /var/lib/docker/image/overlay2/layerdb/sha256/*/diff"
    #print cmd

    result = os.popen( cmd ).read()
    layer_ref = result.split( ':' )[ 0 ]
    layer_ref = layer_ref[:-4] + "cache-id"
    #print "layer_ref: " + layer_ref

    file     = open( layer_ref )
    layer_id = file.read()
    file.close()
    print( "layer_id: " + layer_id )

    layer_path = "/var/lib/docker/overlay2/" + layer_id + "/diff"
    print( "layer_path: " + layer_path )

    print( "" )

    cmd = "ls -al " + layer_path
    print( os.popen( cmd ).read() )

    print( "==========\n" )

print( "### finished" )
print( "### investigating Docker image " + docker_image_name )
print( "############################################################" )

FROM golang:alpine AS builder

ADD ./whoami.go /go/src
ENV GOOS=linux
ENV GOARCH=386
RUN cd /go/src && go build -o /go/bin/whoami
RUN echo "Hallo from Go-Alpine-WebServer" > /go/bin/index.html

FROM alpine
WORKDIR /app
COPY --from=builder /go/bin/whoami /app/
COPY --from=builder /go/bin/index.html /app/src/
ENTRYPOINT [ "./whoami" ]

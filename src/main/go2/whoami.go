package main

import (
  "os"
  "fmt"
  "net/http"
  "log"
)

func ping( w http.ResponseWriter, r *http.Request ) {
  w.Write( []byte ( "pong" ) )
}

func whoami( w http.ResponseWriter, r *http.Request ) {
  hostname, _ := os.Hostname()
  fmt.Fprintf( os.Stdout, "I'm %s\n", hostname )
  fmt.Fprintf( w, "I'm %s\n", hostname )
}

func main() {

  port := os.Getenv( "PORT" )
  if port == "" {
    port = "8000"
  }

  fmt.Fprintf( os.Stdout, "Listening on :%s\n", port )

  http.Handle( "/", http.FileServer( http.Dir( "./src" ) ) )
  http.HandleFunc( "/ping", ping )
  http.HandleFunc( "/whoami", whoami )

  log.Fatal( http.ListenAndServe( ":" + port, nil ) )
}

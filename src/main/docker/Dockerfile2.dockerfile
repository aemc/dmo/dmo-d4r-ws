FROM nginx:alpine

RUN echo "Hallo aus dem Dockerfile" > /usr/share/nginx/html/index.html

ENTRYPOINT [ "/usr/sbin/nginx" ]
CMD [ "-g", "daemon off;" ]

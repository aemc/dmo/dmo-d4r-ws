FROM alpine:latest

RUN mkdir -p /testvol
RUN chown nobody:nogroup /testvol

USER nobody

VOLUME [ "/testvol" ]

ENTRYPOINT [ "sh" ]

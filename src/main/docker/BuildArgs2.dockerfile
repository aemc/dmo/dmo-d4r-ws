FROM alpine

ARG version
ARG license="ASL2.0"
ARG git_commit_id
ARG git_commit_ts

LABEL org.label-schema.schema-version="1.0.0-rc.1" \
      org.label-schema.version=${version} \
      org.label-schema.license=${license} \
      org.label-schema.vcs-ref=${git_commit_id} \
      org.label-schema.vcs-datetime=${git_commit_ts}

RUN env

FROM scratch
WORKDIR /app
COPY --from=aemc/go-hello:v04 /app/hello-app /app/
ENTRYPOINT [ "./hello-app" ]

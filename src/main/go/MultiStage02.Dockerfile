FROM aemc/go-hello:v01

RUN rm -rf /usr/local/go

ENTRYPOINT [ "/go/bin/hello-app" ]

FROM golang:alpine AS builder
WORKDIR /go/src
COPY ./hello.go /go/src
RUN go mod init helloworld
RUN go build -o /go/bin/hello-app

FROM alpine

WORKDIR /app
COPY --from=aemc/go-hello:v01 /go/bin/hello-app /app/

ENTRYPOINT [ "./hello-app" ]

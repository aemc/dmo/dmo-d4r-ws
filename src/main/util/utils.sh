#!/usr/bin/env bash

function printBegin()
{
  local INT_SCRIPT_NAME=$1

  echo "############################################################"
  echo "### $INT_SCRIPT_NAME"
  echo "### begin"
  echo ""
}

function printEnd()
{
  local LOC_SCRIPT_NAME=$1

  echo ""
  echo "### finished"
  echo "### $LOC_SCRIPT_NAME"
  echo "############################################################"
}

function checkGitBranch()
{
  local LOC_SCRIPT_NAME=$1
  local LOC_GIT_EXP_BRANCH=$2
  local LOC_GIT_CUR_BRANCH=$(git rev-parse --abbrev-ref HEAD)

  if [ "${LOC_GIT_CUR_BRANCH}" = "${LOC_GIT_EXP_BRANCH}" ]; then
    echo "=> ok - we are running on git branch ${LOC_GIT_CUR_BRANCH}"
  else
    echo "=> we expect running on git branch ${LOC_GIT_EXP_BRANCH} but we are on ${LOC_GIT_CUR_BRANCH}"
    printEnd ${LOC_SCRIPT_NAME}
    return 1
  fi
}

function getGitCommitId()
{
  local LOC_GIT_COMMIT_ID=$(git show -s --format=%h HEAD)
  echo "${LOC_GIT_COMMIT_ID}"
}

function getGitCommitTimestamp()
{
  local LOC_GIT_COMMIT_TS=$(git show -s --format=%cI HEAD)
  local LOC_GIT_COMMIT_TS=${LOC_GIT_COMMIT_TS//[:,-]/''}
  local LOC_GIT_COMMIT_TS=${LOC_GIT_COMMIT_TS:0:15}
  echo ${LOC_GIT_COMMIT_TS}
}

function printEnv()
{
  echo "DOCKER_HOST_NAME : ${DOCKER_HOST_NAME}"
  echo "DOCKER_HOST      : ${DOCKER_HOST}"
  echo "DOCKER_TLS_VERIFY: ${DOCKER_TLS_VERIFY}"
  echo "DOCKER_CERT_PATH : ${DOCKER_CERT_PATH}"
}

function getOs()
{
  local unameOut="$(uname -s)"
  local machine=""

  case "${unameOut}" in
      Linux*)     machine=Linux;;
      Darwin*)    machine=Mac;;
      CYGWIN*)    machine=Cygwin;;
      MINGW*)     machine=MinGw;;
      *)          machine="UNKNOWN:${unameOut}"
  esac

  echo ${machine}
}

function setDoToken()
{
  local LOC_TOKEN=${2:-}
  local LOC_TOKEN_VAR_NAME=${1:-"PRJ_TOKEN"}
  local LOC_OS=$(getOs)

  if [ -z "$LOC_TOKEN" ]
  then
    echo "PRJ_TOKEN    : <empty> -> try to ask for (password required)"
    case "${LOC_OS}" in
      "Mac" )
        echo "running on: Mac OSX"
        LOC_TOKEN=$(security find-generic-password -s envchain-demo-do -w)
      ;;
      * ) echo "unusable OS - can't set TOKEN for you";;
    esac
    if [ -z "$LOC_TOKEN" ]
    then
      return 1
    else
      echo "PRJ_TOKEN    : got it -> go on ..."
    fi
  fi

  eval ${LOC_TOKEN_VAR_NAME}="'${LOC_TOKEN}'"
}

function callTerraform()
{
  local LOC_RESULT_NAME=${1:-}
  local LOC_RESULT=${2:-}
  local LOC_COMMAND=${3:-"apply"}

  terraform ${LOC_COMMAND} \
    -var "do_token=${PRJ_TOKEN}" \
    -var "cluster_count=${PRJ_NUMBER_OF_CLUSTER}" \
    -var "master_nodes_count=${PRJ_NUMBER_OF_MASTERS}" \
    -var "worker_nodes_count=${PRJ_NUMBER_OF_WORKERS}" \
    -var "prv_key=${PRJ_PKEY_FILE_NAME}" \
    -var "pub_key=${PRJ_PKEY_FILE_NAME}.pub" \
    -var "dns_base_domain=${PRJ_DNS_BASE_DOMAIN}" \
    -var "do_droplet_type=${PRJ_VM_TYPE}" \
    -state=zzTemp.gen/terraform.tfstate \
    src/main/terraform/
  
  LOC_RESULT=${?}

  eval ${LOC_RESULT_NAME}="'${LOC_RESULT}'"
}

#!/usr/bin/env bash

SCRIPT_NAME="sourceDockerEnv"

source src/main/util/utils.sh

printBegin ${SCRIPT_NAME}

case "$#" in
"0"|"1"*)
    echo "script needs two parameters: "
    echo "   folder containing Docker client certs (i.e. ~/.d4r/dws)"
    echo "   node to connect to (i.e. 'm01.cluster01.aemc.me')"
    echo ""
    echo "can't do anything for you -> exit"
    echo ""
    ;;
"2"*)
    echo "export necessary environment variable"
    echo ""
    export DOCKER_HOST_NAME="${2}"
    export DOCKER_HOST="tcp://${DOCKER_HOST_NAME}:2376"
    export DOCKER_TLS_VERIFY="1"
    export DOCKER_CERT_PATH="${1}"
    ;;
*)
    echo "too much parameters"
    ;;
esac

printEnv

printEnd ${SCRIPT_NAME}
